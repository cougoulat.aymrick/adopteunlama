<?php
session_start();

require_once('./libs/smarty/Smarty.class.php');

/*
    Connexion de base

    $user = "debian-sys-maint"
    $pass = "aR7RIRZbiUZw3dYk"
*/

// CONSTANT VARIABLES
$smarty = new Smarty();
$smarty->template_dir = 'views';
$smarty->compile_dir = 'tmp';


try {
    $bdd = new PDO("mysql:host=localhost;dbname=webapp", "root",  "");
    $bdd->query("SET NAMES UTF8");
} catch (Exception $e) {
    echo "Problème de connexion à la base de donnée !";
    die();
}

$smarty->display('test.html');

