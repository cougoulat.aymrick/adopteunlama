#  Projet TIDAL : AdopteUnLama.com

## Différentes sources :

Compréhension du langage PHP : 
    - Documents données sur e-campus
    - Documentation PHP : https://www.php.net/docs.php
    - Tutoriel Open-Classroom : https://openclassrooms.com/courses/concevez-votre-site-web-avec-php-et-mysql

Compréhension de l'architecture MVC avec PHP :
    - Tutoriel Open-Classroom : https://openclassrooms.com/fr/courses/4670706-adoptez-une-architecture-mvc-en-php

Compréhension sur l'utilisation de la librairie Smarty :
    - https://www.smarty.net/documentation
    - Tutoriel developpez.com : https://eric-pommereau.developpez.com/tutoriels/initiation-smarty/?page=page_1

Documentation sur le framework CSS UiKit :
    - Site web officiel : https://www.smarty.net/documentation
    
