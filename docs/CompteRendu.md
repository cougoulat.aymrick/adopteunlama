#  Projet TIDAL : AdopteUnLama.com


## Le 01/10/20 : 

 - **Prévu :** 
	 - Découverte du contexte projet et des demandes.
	 - Réflexion sur l'architecture et l'organisation.

 - **Fait :**
	 - Première réflexion sur l'architecture et découverte des technologies PHP et Smarty.
	 - Choix d'un outils de collaboration et d'organisation du projet : ClickUp
	 - Création du dépôt GitHub.

 - **A faire la prochaine fois :**
	 - Prototype visuel de l'application et des différents affichages
	 - Elaboration des tâches dans ClickUp et assignation de ces mêmes tâches
	 - Configuration des environnements de développement fournis
	 - Création de l'architecture basique de l'application

 - **Problèmes :**
	 - Pas de problèmes en particulier malgrès quelques peurs sur les technologies et l'architecture MVC avec PHP.


## Le 15/10/20 : 

 - **Prévu :** 
	 - Prototype visuel de l'application et des différents affichages
	 - Elaboration des tâches dans ClickUp et assignation de ces mêmes tâches
	 - Configuration des environnements de développement fournis
	 - Création de l'architecture basique de l'application
	 - Qualification du script de chargement de la base de donnée


 - **Fait :**
 	- Qualification du script de chargement de la base de donnée


 - **A faire la prochaine fois :**


 - **Problèmes :**

